use ines::Ines;
use std::fs::File;

struct Args {
    rom_path: String,
}

impl Args {
    fn parser() -> impl meap::Parser<Item = Self> {
        meap::let_map! {
            let {
                rom_path = pos_req("PATH");
            } in {
                Self { rom_path }
            }
        }
    }
}

fn ines_from_file(path: &str) -> Ines {
    use std::io::Read;
    let mut input = Vec::new();
    let mut rom_file = File::open(path).expect("Failed to open rom file");
    rom_file.read_to_end(&mut input).unwrap();
    Ines::parse(&input).unwrap()
}

fn main() {
    use meap::Parser;
    let args = Args::parser().with_help_default().parse_env_or_exit();
    let ines = ines_from_file(args.rom_path.as_str());
    let chr = &ines.chr_rom;
    let chars = " !\"#&'(),-./0123456789:?ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        .chars()
        .collect::<Vec<_>>();
    println!("#[rustfmt::skip]");
    println!("pub const CHR: [u8; {}] = [", chars.len() * 48);
    for (i, &ch) in chars.iter().enumerate() {
        let base = i * 16;
        println!("");
        println!("    // {}: {}", i, ch);
        for i in 8..16 {
            let chr_byte = chr[base + i];
            println!("    0b{:08b},", chr_byte);
        }
        for _ in 0..8 {
            println!("    0b{:08b},", 0);
        }
        for _ in 0..8 {
            println!("    0b{:08b},", 0);
        }
        for i in 8..16 {
            let chr_byte = chr[base + i];
            println!("    0b{:08b},", chr_byte);
        }
        for i in 8..16 {
            let chr_byte = chr[base + i];
            println!("    0b{:08b},", chr_byte);
        }
        for i in 8..16 {
            let chr_byte = chr[base + i];
            println!("    0b{:08b},", chr_byte);
        }
    }
    println!("");
    println!("];");
    println!("");
    println!("#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]");
    println!("pub enum Style {{");
    println!("    S1,");
    println!("    S2,");
    println!("    S3,");
    println!("}}");
    println!("");
    println!("#[rustfmt::skip]");
    println!(
        "pub const LUT: [((char, Style), u8); {}] = [",
        chars.len() * 3
    );
    for (i, &ch) in chars.iter().enumerate() {
        let ch_str = if ch == '\'' {
            format!("\\'")
        } else {
            format!("{}", ch)
        };
        println!("    (('{}', Style::S1), {}),", ch_str, (i * 3) + 0);
        println!("    (('{}', Style::S2), {}),", ch_str, (i * 3) + 1);
        println!("    (('{}', Style::S3), {}),", ch_str, (i * 3) + 2);
    }
    println!("];");
}
